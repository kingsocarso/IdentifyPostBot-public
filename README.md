# IdentifyPostBot-public

Public release of source code behind IdentifyPostBot. Note that this is periodically merged from a separate working repo which will remain private since it contains Reddit credentials.

Aids in the policing of "Identify" posts. The bot does this by looking for "trigger" words or phrases in post titles; if two or more are found, a friendly reply is made and the post is reported to the mods.

Ideally, this will speed up moderation a bit on the r/ArtHistory subreddit.