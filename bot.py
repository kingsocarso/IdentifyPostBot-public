import praw

r = praw.Reddit(user_agent = 'IdentifyPostBot v1.0 by kingsocarso', client_id = 'your_id_here', client_secret = 'OAauth_goes_here', username = 'username', password = 'password')
subreddit = r.subreddit('ArtHistory')

TRIGGERS = ['garage sale', 'estate sale', 'yard sale', 'how much', 'worth', 'signature', 'antique', 'recognize', 'familiar']
triggerCount = 0

posts = subreddit.stream.submissions() #monitor new posts

for post in posts:
    for trigger in TRIGGERS:
        if trigger in post.title.lower(): #look for triggers in normalized post titles
            triggerCount += 1
    if triggerCount >= 2: #take action if more than two triggers are found, creating a preponderance of evidence
        post.reply("It looks like you're trying to determine the value or authenticity of a recently acquired work. Unfortunately, this is not allowed on this subreddit unless it focuses on thoughtful discussion. Please redirect your inquiry to r/WhatIsThisPainting.\n\nThank you.\n\n*I am a bot; please message the other moderators for help.*")
        subreddit.message('u/' + post.author.name + "'s post appears to ask for appraisal", "Please check if " + post.url + " should be removed.")
        print("Policed " + post.url)
    triggerCount = 0
